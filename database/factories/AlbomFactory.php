<?php

namespace Database\Factories;

use App\Models\Albom;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbomFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Albom::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
//            'user_id' => rand(1,10),
            'title' => $this->faker->word,
        ];
    }

//    public function configure()
//    {
//        return $this->afterMaking(function (Albom $albom) {
//            \App\Models\Photo::factory(100)->create(['albom_id' => $albom->id]);
//        });
//    }
}
