<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create()->each(function ($user) {
            \App\Models\Albom::factory(10)->create(['user_id' => $user->id])->each(function ($albom) {
                \App\Models\Photo::factory(100)->create(['albom_id' => $albom->id]);
            });
        });
    }
}
