<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\{
    UserController,
    AlbomController,
};

Route::get('/users', [UserController::class, 'index']);
Route::get('/users/{user}', [UserController::class, 'show']);

Route::get('/albums', [AlbomController::class, 'index']);
Route::get('/albums/{albom}', [AlbomController::class, 'show']);
