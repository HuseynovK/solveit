<?php


namespace App\Service;


use Illuminate\Database\Eloquent\Model;

class BaseServise
{
    public $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
