<?php

namespace App\Service;

use App\Http\Resources\AlbomResource;
use App\Http\Resources\UserResource;
use App\Models\Book;
use App\Models\User;
use App\Service\BaseServise;

class UserService extends BaseServise
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        return UserResource::collection($this->model->select('id', 'first_name', 'last_name')->with('alboms')->orderBy('id')->paginate(10));
    }

    public function getOne($user)
    {
        return (new UserResource($user->load('alboms')));
    }
}
