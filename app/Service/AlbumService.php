<?php

namespace App\Service;

use App\Http\Resources\AlbomResource;
use App\Http\Resources\AlbumDetailResource;
use App\Http\Resources\UserResource;
use App\Models\Albom;
use App\Models\User;
use App\Service\BaseServise;

class AlbumService extends BaseServise
{
    public function __construct(Albom $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        return AlbomResource::collection($this->model->select('id', 'title')->paginate(10));
    }

    public function getOne($albom)
    {
        return (new AlbumDetailResource($albom->load(['user', 'photos'])));
    }
}
