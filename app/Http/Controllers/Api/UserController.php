<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Service\UserService;

class UserController extends Controller
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        return response()->send_with_data($this->userService->getAll());
    }

    public function show(User $user)
    {
        return response()->send_with_data($this->userService->getOne($user));
    }
}
