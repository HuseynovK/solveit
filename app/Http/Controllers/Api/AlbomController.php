<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Albom;
use App\Service\AlbumService;

class AlbomController extends Controller
{
    public $albumService;

    public function __construct(AlbumService $albumService)
    {
        $this->albumService = $albumService;
    }

    public function index()
    {
        return response()->send_with_data($this->albumService->getAll());
    }

    public function show(Albom $albom)
    {
        return response()->send_with_data($this->albumService->getOne($albom));
    }
}
