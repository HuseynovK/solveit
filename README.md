<h1>Api Documentation</h1>
https://documenter.getpostman.com/view/13600004/UUy39mV9

<h1>Project Configuration</h1>

composer install

copy .env.example -> .env 

create variable in env file APP_PASSWORD=your_password

php artisan key:generate

write your database name, username and password to env file

php artisan migrate --seed

php artisan serve - to run project

